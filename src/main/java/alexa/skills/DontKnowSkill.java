package alexa.skills;

/**
 * Created by amen on 9/14/17.
 */
public class DontKnowSkill extends AbstractSkill {

    public DontKnowSkill() {
    }

    public void run() {
        System.out.println("ALEXA: Sorry, i don't know that.");
    }
}
