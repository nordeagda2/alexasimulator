package alexa.skills;

import alexa.AlexaAccountInfo;

import java.util.Observable;

/**
 * Created by amen on 9/14/17.
 */
public abstract class AbstractSkill extends Observable implements Runnable {
    protected AlexaAccountInfo accountInfo;

    public void invokeUsing(AlexaAccountInfo info) {
        accountInfo = info;
    }

}
