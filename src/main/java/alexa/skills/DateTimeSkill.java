package alexa.skills;

import java.time.LocalDateTime;

/**
 * Created by amen on 9/14/17.
 */
public class DateTimeSkill extends AbstractSkill {
    public DateTimeSkill() {
    }

    public void run() {
        System.out.println("ALEXA: the time is " + LocalDateTime.now());
    }
}
