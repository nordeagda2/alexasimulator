package alexa.skills;

/**
 * Created by amen on 9/14/17.
 */
public class AddToDoTaskSkill extends AbstractSkill {
    private String task;

    public AddToDoTaskSkill(String task) {
        this.task = task;
    }

    public void run() {
        if (!task.trim().isEmpty()) {
            accountInfo.getTasks().add(task);
            System.out.println("ALEXA: added \"" + task + "\" to todo tasks.");
        } else {
            System.out.println("ALEXA: Say the task You want to add");
        }
        // todo akcja
    }
}
