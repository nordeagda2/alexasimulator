package alexa.skills;

import java.util.Iterator;
import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class RemoveToDoTaskSkill extends AbstractSkill {
    private String task;

    public RemoveToDoTaskSkill(String task) {
        this.task = task;
    }

    public void run() {
        List<String> tasks = accountInfo.getTasks();
        Iterator<String> it = tasks.iterator();
        while (it.hasNext()) {
            String task = it.next();
            if (task.equalsIgnoreCase(this.task)) {
                System.out.println("ALEXA: task has been removed.");
                it.remove();
                return;
            }
        }

        System.out.println("ALEXA: there is no \"" + this.task + "\" to remove.");
    }
}
