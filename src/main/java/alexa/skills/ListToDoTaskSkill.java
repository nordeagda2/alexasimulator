package alexa.skills;

import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class ListToDoTaskSkill extends AbstractSkill {

    public void run() {
        List<String> tasks = accountInfo.getTasks();
        System.out.println("ALEXA: tasks are:");
        tasks.stream().forEach((task) -> {
            System.out.println(task);
        });
        System.out.println("ALEXA: those are all tasks.");
//        for (String task : tasks) {
//            System.out.println(task);
//        }
    }
}
