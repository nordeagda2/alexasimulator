package alexa;

/**
 * Created by amen on 9/14/17.
 */
public class Request {
    private String request;
    private AlexaDevice deviceToCall;

    public Request(String request, AlexaDevice deviceToCall) {
        this.request = request;
        this.deviceToCall = deviceToCall;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public AlexaDevice getDeviceToCall() {
        return deviceToCall;
    }

    public void setDeviceToCall(AlexaDevice deviceToCall) {
        this.deviceToCall = deviceToCall;
    }
}
